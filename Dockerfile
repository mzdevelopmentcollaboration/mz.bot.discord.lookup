FROM mcr.microsoft.com/dotnet/runtime:6.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Mz.Bot.Discord.Lookup/Mz.Bot.Discord.Lookup.csproj", "Mz.Bot.Discord.Lookup/"]
RUN dotnet restore "Mz.Bot.Discord.Lookup/Mz.Bot.Discord.Lookup.csproj"
COPY . .
WORKDIR "/src/Mz.Bot.Discord.Lookup"
RUN dotnet build "Mz.Bot.Discord.Lookup.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Mz.Bot.Discord.Lookup.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

RUN apt-get update && apt-get install -y xorg openbox libnss3 libasound2

ENTRYPOINT ["dotnet", "Mz.Bot.Discord.Lookup.dll"]
