<h1 style="text-align: center;">Kuebiko - A Network Testing Tool for Discord</h1>

> This project is still under heavy development. Any current issues are planned to be fixed. Additional design choices and features will be updated and are therefore bound to change.

Kuebiko is a simple network testing tool, which can be utilized in form of a bot for Discord. Its premise is to deliver a quick and accessible way to gather some basic information about a network resource of interest.

## Functionality

Kuebiko currently offers the following two commands, with several more planed for the future.

- "fetch-host-details" - Offers details for a provided IP or hostname
- "check-open-ports" - Checks for open common predefined ports on a given IP or hostname

## Utilized Projects

Within Kuebiko, three main projects from the open-source community are used:

- [Discord.Net-Labs](https://github.com/Discord-Net-Labs/Discord.Net-Labs) by [Quin Lynch](https://github.com/quinchs)
- [puppeteer-sharp](https://github.com/hardkoded/puppeteer-sharp) by [hardkoded](https://github.com/hardkoded)
- [html-agility-pack](https://github.com/zzzprojects/html-agility-pack) by [Jonathan Magnan](https://github.com/JonathanMagnan)

## How it works

To circumvent the need for REST APIs to gather all necessary information, Kuebiko relies on web scraping. Although not being the cleanest and fastest option, this way still provides the most flexible way of gathering data, as everything can be easily scraped from web pages. Based on that fact, Kuebiko basically executes the following tasks for each request:

1. Checks the input for an IP or a hostname
2. Open a new tab within a headless Chromium browser
3. Parse all data via the html-agility-pack from pulled data
4. Responds to the request on Discord with the gathered information

Within this process are several more steps, but this is the basic gist of the work.

## Additional Remark

As stated at the beginning, this project is still under heavy development. Feedback or criticism can be issued in form of an issue.