﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace Mz.Bot.Discord.Lookup.Tests
{
    public class ParseTests
    {
        [Fact]
        public void ValidateIp()
        {
            if (!Regex.IsMatch("127.0.0.1",
                    "^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)(\\.(?!$)|$)){4}$"))
            {
                throw new FormatException("Not a valid IP address");
            }
        }
    }
}
