﻿using Xunit;
using System;
using Xunit.Abstractions;

namespace Mz.Bot.Discord.Lookup.Tests;

public class SelectionTests
{
    private readonly ITestOutputHelper _outputHelper;

    public SelectionTests(ITestOutputHelper outputHelper)
    {
        _outputHelper = outputHelper;
    }

    [Fact]
    public void CheckRandomSelection()
    {
        var random = new Random();
        var randomInteger = random.Next(0, 2);
        // ReSharper disable once ConvertSwitchStatementToSwitchExpression
        switch (randomInteger)
        {
            case 0:
                _outputHelper.WriteLine(randomInteger.ToString());
                break;
            case 1:
                _outputHelper.WriteLine(randomInteger.ToString());
                break;
            default:
                throw new ArgumentException("Unexpected value");
        }
    }
}
