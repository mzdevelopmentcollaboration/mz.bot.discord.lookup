using Discord;
using PuppeteerSharp;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Mz.Bot.Discord.Lookup.Services;

namespace Mz.Bot.Discord.Lookup;

internal class Startup
{
    public IConfigurationRoot Configuration { get; }

    public Startup()
    {
        var configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile("config.json", false);
        Configuration = configurationBuilder.Build();
    }

    public static async Task RunAsync(string[] args)
    {
        var startup = new Startup();
        await startup.RunAsync();
    }

    public async Task RunAsync()
    {
        var services = new ServiceCollection();
        await ConfigureServicesAsync(services);
        var serviceProvider = services.BuildServiceProvider();
        serviceProvider.GetRequiredService<BrowserService>();
        serviceProvider.GetRequiredService<LoggingService>();
        serviceProvider.GetRequiredService<SlashCommandService>();
        await serviceProvider.GetRequiredService<StartupService>().StartAsync();
        await Task.Delay(-1);
    }

    private async Task ConfigureServicesAsync(IServiceCollection services)
    {
        using var browserFetcher = new BrowserFetcher();
        await browserFetcher.DownloadAsync();
        var launchOptions = new LaunchOptions
        {
            Headless = true,
            Args = new[] { "--no-sandbox" }
        };
        var browser = await Puppeteer.LaunchAsync(launchOptions);
        services.AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
        {
            LogLevel = LogSeverity.Verbose,
            MessageCacheSize = 250
        }))
        .AddSingleton(new CommandService(new CommandServiceConfig
        {
            LogLevel = LogSeverity.Verbose,
            DefaultRunMode = RunMode.Async
        }))
        .AddSingleton<SlashCommandService>()
        .AddSingleton<StartupService>()
        .AddSingleton<LoggingService>()
        .AddSingleton(new BrowserService(browser))
        .AddSingleton(Configuration);
    }
}
