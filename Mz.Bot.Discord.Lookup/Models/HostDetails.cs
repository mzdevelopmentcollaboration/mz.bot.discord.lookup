﻿namespace Mz.Bot.Discord.Lookup.Models
{
    internal class HostDetails
    {
        public string? City { get; set; }
        public string? Country { get; set; }
        public string? Organization { get; set; }
        public string? Isp { get; set; }
        public string? Source { get; set; }
    }
}
