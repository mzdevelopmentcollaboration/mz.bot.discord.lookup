﻿namespace Mz.Bot.Discord.Lookup.Models;

internal class SlashCommand
{
    public string? Name { get; set; }
    public string? Description { get; set; }
    public List<SlashCommandOption>? Options { get; set; }

    internal class SlashCommandOption
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string? OptionType { get; set; }
    }
}
