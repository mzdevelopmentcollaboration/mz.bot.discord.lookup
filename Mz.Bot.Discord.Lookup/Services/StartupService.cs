﻿using Discord;
using Discord.WebSocket;

namespace Mz.Bot.Discord.Lookup.Services
{
    internal class StartupService
    {
        private readonly DiscordSocketClient _discordSocketClient;

        public StartupService(DiscordSocketClient discordSocketClient)
        {
            _discordSocketClient = discordSocketClient;
        }

        public async Task StartAsync()
        {
            var botToken = Environment.GetEnvironmentVariable("DISCORD_BOT_TOKEN");
            await _discordSocketClient.LoginAsync(TokenType.Bot, botToken);
            await _discordSocketClient.StartAsync();
            await _discordSocketClient.SetActivityAsync(new Game("Chromium", ActivityType.Watching));
        }
    }
}
