﻿using Discord;
using Discord.WebSocket;

namespace Mz.Bot.Discord.Lookup.Services;

internal class LoggingService
{

    // ReSharper disable once SuggestBaseTypeForParameterInConstructor
    public LoggingService(DiscordSocketClient discordSocketClient)
    {
        discordSocketClient.Log += DiscordSocketClientOnLog;
    }

    // ReSharper disable once MemberCanBeMadeStatic.Local
    private Task DiscordSocketClientOnLog(LogMessage logMessage)
    {
        var formattedLogMessage =
            $"{DateTime.UtcNow.ToString("hh:mm:ss")} [{logMessage.Severity}] {logMessage.Source}: {logMessage.Exception?.ToString() ?? logMessage.Message}";
        return Console.Out.WriteLineAsync(formattedLogMessage);
    }
}
