﻿using PuppeteerSharp;

using Mz.Bot.Discord.Lookup.Models;
using Mz.Bot.Discord.Lookup.Managers;

namespace Mz.Bot.Discord.Lookup.Services;

internal class BrowserService
{
    private readonly Browser _browser;
    private readonly ResourceType[] _resources;
    private readonly ParseManager _parseManager;

    public BrowserService(Browser browser)
    {
        _browser = browser;
        _parseManager = new ParseManager();
        _resources = new[]
        {
            ResourceType.Image,
            ResourceType.Img,
            ResourceType.Media,
            ResourceType.StyleSheet
        };
    }

    public async Task<HostDetails> FetchIpDetailsAsync(string ipAddress)
    {
        var random = new Random();
        var randomInteger = random.Next(0, 2);
        string body;
        var parsedBody = new HostDetails();
        switch (randomInteger)
        {
            case 0:
                body = await PerformPageEvaluationAsync($"https://whatismyipaddress.com/ip/{ipAddress}");
                parsedBody = _parseManager.ParseWhatIsMyIpAddressBody(body);
                break;
            case 1:
                body = await PerformPageEvaluationAsync($"https://ip.me/?ip={ipAddress}");
                parsedBody = _parseManager.ParseIpMeBody(body);
                break;
        }
        return parsedBody;
    }

    private async Task<string> PerformPageEvaluationAsync(string url)
    {
        var page = await CreatePageAsync();
        await page.SetRequestInterceptionAsync(true);
        page.Request += PageOnRequestAsync;
        await page.GoToAsync(url);
        var body = await page.GetContentAsync();
        await page.DisposeAsync();
        return body;
    }

    private async void PageOnRequestAsync(object? sender, RequestEventArgs requestEventArguments)
    {
        if (_resources.Contains(requestEventArguments.Request.ResourceType))
        {
            await requestEventArguments.Request.AbortAsync();
            return;
        }
        await requestEventArguments.Request.ContinueAsync();
    }

    private async Task<Page> CreatePageAsync()
    {
        var page = await _browser.NewPageAsync();
        var random = new Random();
        var randomInteger = Convert.ToInt32(random.Next(0, Puppeteer.Devices.Count));
        await page.EmulateAsync(Puppeteer.Devices.ElementAt(randomInteger).Value);
        return page;
    }
}
