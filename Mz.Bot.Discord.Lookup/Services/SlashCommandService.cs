﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;

using Mz.Bot.Discord.Lookup.Models;
using Mz.Bot.Discord.Lookup.Managers;

namespace Mz.Bot.Discord.Lookup.Services;

internal class SlashCommandService
{
    private readonly CommandManager _commandManager;
    private readonly IConfigurationRoot _configurationRoot;
    private readonly DiscordSocketClient _discordSocketClient;

    // ReSharper disable once SuggestBaseTypeForParameterInConstructor
    public SlashCommandService(DiscordSocketClient discordSocketClient,
        IConfigurationRoot configurationRoot, BrowserService browserService)
    {
        _configurationRoot = configurationRoot;
        _discordSocketClient = discordSocketClient;
        _commandManager = new CommandManager(browserService);
        _discordSocketClient.Ready += DiscordSocketClientOnReady;
        _discordSocketClient.SlashCommandExecuted += DiscordSocketClientOnSlashCommandExecuted;
    }

    private async Task DiscordSocketClientOnReady()
    {
        var guild = _discordSocketClient.GetGuild(794617695840108544);
        // ReSharper disable once ForeachCanBePartlyConvertedToQueryUsingAnotherGetEnumerator
        foreach (var slashCommand in _configurationRoot
            .GetSection("Discord:SlashCommands").Get<List<SlashCommand>>()) 
        {
            var guildSlashCommandBuilder = new SlashCommandBuilder();
            guildSlashCommandBuilder.WithName(slashCommand.Name);
            guildSlashCommandBuilder.WithDescription(slashCommand.Description);
            if (slashCommand.Options != null && slashCommand.Options.Any())
            {
                foreach (var slashCommandOption in slashCommand.Options)
                {
                    var slashCommandOptionBuilder = new SlashCommandOptionBuilder();
                    slashCommandOptionBuilder.WithName(slashCommandOption.Name);
                    slashCommandOptionBuilder.WithDescription(slashCommandOption.Description);
                    switch (slashCommandOption.OptionType)
                    {
                        case "String":
                            slashCommandOptionBuilder.WithType(ApplicationCommandOptionType.String);
                            break;
                        default:
                            slashCommandOptionBuilder.WithType(ApplicationCommandOptionType.String);
                            break;
                    }
                    guildSlashCommandBuilder.AddOption(slashCommandOptionBuilder);
                }
            }
            var guildSlashCommand = guildSlashCommandBuilder.Build();
            await guild.CreateApplicationCommandAsync(guildSlashCommand);
        }
    }

    // ReSharper disable once MemberCanBeMadeStatic.Local
    private async Task DiscordSocketClientOnSlashCommandExecuted(SocketSlashCommand socketSlashCommand)
    {
        switch (socketSlashCommand.Data.Name)
        {
            case "fetch-host-details":
                await _commandManager.FetchIpDetailsAsync(socketSlashCommand);
                break;
            case "check-open-ports":
                await _commandManager.CheckOpenPortsAsync(socketSlashCommand);
                break;
        }
    }
}
