﻿using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace Mz.Bot.Discord.Lookup.Managers;

internal class ConnectionManager
{
    private readonly int[] _toBeCheckedPorts;

    public ConnectionManager()
    {
        _toBeCheckedPorts = new[]
        {
            443,
            80,
            25
        };
    }

    public async Task<string> PingHostAddressAsync(string hostnameOrIp)
    {
        var ipAddresses = await Dns.GetHostAddressesAsync(hostnameOrIp);
        ipAddresses = ipAddresses.Where(x => x.AddressFamily == AddressFamily.InterNetwork).ToArray();
        var ping = new Ping();
        var ipAddress = ipAddresses.FirstOrDefault() ??
            throw new InvalidOperationException("Hostname could not be resolved");
        await ping.SendPingAsync(ipAddress);
        return ipAddress.ToString();
    }

    public List<int> CheckPortAvailability(IPAddress ipAddress)
    {
        var openPorts = new List<int>();
        foreach (var toBeCheckedPort in _toBeCheckedPorts)
        {
            var tcpClient = new TcpClient();
            var tcpConnection = tcpClient.BeginConnect(ipAddress, toBeCheckedPort, null, null);
            var isConnected = tcpConnection.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));
            if (isConnected)
            {
                openPorts.Add(toBeCheckedPort);
            }
            tcpClient.Dispose();
        }
        return openPorts;
    }
}
