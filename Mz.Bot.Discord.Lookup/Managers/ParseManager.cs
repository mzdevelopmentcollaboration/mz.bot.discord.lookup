﻿using System.Text.RegularExpressions;

using HtmlAgilityPack;

using Mz.Bot.Discord.Lookup.Models;

namespace Mz.Bot.Discord.Lookup.Managers;

internal class ParseManager
{
    public string ParseHost(string hostnameOrIp)
    {
        var isIp = Regex.IsMatch(hostnameOrIp, "^((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)(\\.(?!$)|$)){4}$");
        if (isIp)
        {
            return hostnameOrIp;
        }
        throw new FormatException("Provided IP address is likely not valid");
    }

    // TODO: Create specific return type
    public HostDetails ParseIpMeBody(string body)
    {
        var htmlDocument = new HtmlDocument();
        htmlDocument.LoadHtml(body);
        var countryHtmlNode =
            htmlDocument.DocumentNode.SelectSingleNode("/html/body/main/div/div[1]/div[2]/table/tbody/tr[2]/td/code");
        var cityHtmlNode =
            htmlDocument.DocumentNode.SelectSingleNode("/html/body/main/div/div[1]/div[2]/table/tbody/tr[1]/td/code");
        var ispHtmlNode =
            htmlDocument.DocumentNode.SelectSingleNode("/html/body/main/div/div[1]/div[2]/table/tbody/tr[9]/td/code");
        var organizationHtmlNode =
            htmlDocument.DocumentNode.SelectSingleNode("/html/body/main/div/div[1]/div[2]/table/tbody/tr[7]/td/code");
        var hostDetails = new HostDetails
        {
            City = cityHtmlNode.InnerText,
            Country = countryHtmlNode.InnerText,
            Isp = ispHtmlNode.InnerText,
            Organization = organizationHtmlNode.InnerText,
            Source = "ip.me"
        };
        return hostDetails;
    }

    // TODO: Create specific return type
    public HostDetails ParseWhatIsMyIpAddressBody(string body)
    {
        var htmlDocument = new HtmlDocument();
        htmlDocument.LoadHtml(body);
        var countryHtmlNode = htmlDocument.DocumentNode.SelectSingleNode(
            "/html/body/div/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div[1]/p[10]/span[2]");
        var cityHtmlNode = htmlDocument.DocumentNode.SelectSingleNode(
            "/html/body/div[2]/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div[1]/p[12]/span[2]");
        var ispHtmlNode = htmlDocument.DocumentNode.SelectSingleNode(
            "/html/body/div[2]/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div[1]/p[4]/span[2]");
        var organizationHtmlNode = htmlDocument.DocumentNode.SelectSingleNode(
            "/html/body/div[2]/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div[1]/div[1]/p[5]/span[2]");
        var hostDetails = new HostDetails
        {
            City = cityHtmlNode.InnerText,
            Country = countryHtmlNode.InnerText,
            Isp = ispHtmlNode.InnerText,
            Organization = organizationHtmlNode.InnerText,
            Source = "whatismyipaddress.com"
        };
        return hostDetails;
    }
}
