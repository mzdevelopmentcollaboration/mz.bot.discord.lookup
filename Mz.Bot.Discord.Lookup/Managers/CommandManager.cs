﻿using System.Net;
using Discord.WebSocket;

using Mz.Bot.Discord.Lookup.Services;

namespace Mz.Bot.Discord.Lookup.Managers;

internal class CommandManager
{
    private readonly ParseManager _parseManager;
    private readonly EmbedManager _embedManager;
    private readonly BrowserService _browserService;
    private readonly ConnectionManager _connectionManager;

    public CommandManager(BrowserService browserService)
    {
        _browserService = browserService;
        _parseManager = new ParseManager();
        _embedManager = new EmbedManager();
        _connectionManager = new ConnectionManager();
    }

    public async Task CheckOpenPortsAsync(SocketSlashCommand socketSlashCommand)
    {
        await socketSlashCommand.DeferAsync();
        var hostnameOrIp = socketSlashCommand.Data.Options.First().Value.ToString();
        string parsedHost;
        IPAddress parsedHostIp;
        try
        {
            parsedHost = _parseManager.ParseHost(hostnameOrIp!);
            parsedHostIp = IPAddress.Parse(parsedHost);
            var openPorts = _connectionManager.CheckPortAvailability(parsedHostIp);
            await socketSlashCommand.ModifyOriginalResponseAsync(messageProperties =>
            {
                messageProperties.Embed = _embedManager.GenerateOpenPortDetailsResponse(openPorts);
            });
        }
        catch (Exception)
        {
            try
            {
                parsedHost = await _connectionManager.PingHostAddressAsync(hostnameOrIp!);
                parsedHostIp = IPAddress.Parse(parsedHost);
                var openPorts = _connectionManager.CheckPortAvailability(parsedHostIp);
                await socketSlashCommand.ModifyOriginalResponseAsync(messageProperties =>
                {
                    messageProperties.Embed = _embedManager.GenerateOpenPortDetailsResponse(openPorts);
                });
            }
            catch (Exception e)
            {
                await socketSlashCommand.ModifyOriginalResponseAsync(messageProperties =>
                {
                    messageProperties.Content = e.Message;
                });
            }
        }
    }

    public async Task FetchIpDetailsAsync(SocketSlashCommand socketSlashCommand)
    {
        await socketSlashCommand.DeferAsync();
        var hostnameOrIp = socketSlashCommand.Data.Options.First().Value.ToString();
        string parsedHost;
        try
        {
            parsedHost = _parseManager.ParseHost(hostnameOrIp!);
            var parsedBody = await _browserService.FetchIpDetailsAsync(parsedHost);
            await socketSlashCommand.ModifyOriginalResponseAsync(messageProperties =>
            {
                messageProperties.Embed = _embedManager.GenerateHostDetailsResponse(parsedBody);
            });
        }
        catch (Exception)
        {
            try
            {
                parsedHost = await _connectionManager.PingHostAddressAsync(hostnameOrIp!);
                var parsedBody = await _browserService.FetchIpDetailsAsync(parsedHost);
                await socketSlashCommand.ModifyOriginalResponseAsync(messageProperties =>
                {
                    messageProperties.Embed = _embedManager.GenerateHostDetailsResponse(parsedBody);
                });
            }
            catch (Exception e)
            {
                await socketSlashCommand.ModifyOriginalResponseAsync(messageProperties =>
                {
                    messageProperties.Content = e.Message;
                });
            }
        }
    }
}
