﻿using Discord;

using Mz.Bot.Discord.Lookup.Models;

namespace Mz.Bot.Discord.Lookup.Managers
{
    internal class EmbedManager
    {
        // TODO: Do this in a smarter way
        public Embed GenerateHostDetailsResponse(HostDetails hostDetails)
        {
            var embed = new EmbedBuilder();
            embed.WithTitle("Title");
            embed.WithUrl("https://mzdevelopment.eu");
            embed.WithDescription("Description");
            embed.WithColor(Color.Red);
            var countryField = new EmbedFieldBuilder
            {
                Name = "Country",
                Value = hostDetails.Country
            };
            embed.AddField(countryField);
            var cityField = new EmbedFieldBuilder
            {
                Name = "City",
                Value = hostDetails.City
            };
            embed.AddField(cityField);
            var ispField = new EmbedFieldBuilder
            {
                Name = "ISP",
                Value = hostDetails.Isp
            };
            embed.AddField(ispField);
            var organizationField = new EmbedFieldBuilder
            {
                Name = "Organization",
                Value = hostDetails.Organization
            };
            embed.AddField(organizationField);
            var sourceField = new EmbedFieldBuilder
            {
                Name = "Source",
                Value = hostDetails.Source
            };
            embed.AddField(sourceField);
            return embed.Build();
        }

        public Embed GenerateOpenPortDetailsResponse(IEnumerable<int> openPorts)
        {
            var embed = new EmbedBuilder();
            embed.WithTitle("Title");
            embed.WithUrl("https://mzdevelopment.eu");
            embed.WithDescription("Description");
            embed.WithColor(Color.Red);
            var parsedOpenPorts = string.Empty;
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var openPort in openPorts)
            {
                parsedOpenPorts += openPort + "\n";
            }
            var portsField = new EmbedFieldBuilder
            {
                Name = "Ports",
                Value = parsedOpenPorts
            };
            embed.AddField(portsField);
            return embed.Build();
        }
    }
}
